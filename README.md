# SCRIPT : STOCKER LE **TFSTATE** DE TERRAFORM DANS UN CONTENEUR AZURE STORAGE

## Prérequis :

- Avoir un compte [Azure](https://portal.azure.com/)
- Avoir installé [Azure CLI](https://docs.microsoft.com/fr-fr/cli/azure/install-azure-cli)
- se connecter à Azure en utilisant la commande `az login`.
- Avoir installé [Curl](https://curl.se/download.html) pour obtenir l'adresse IP publique du poste.

## Description :

Ce script crée un compte de stockage Azure, un conteneur et un fichier backend.tf pour stocker le tfstate de Terraform.


## Utilisation :

Télécharger ou copier le script backend.sh dans le répertoire où se trouve votre code Terraform.

Rendre le script exécutable en utilisant la commande `chmod +x backend.sh`

puis exécuter le script en utilisant la commande `./backend.sh`

**Ce script est interactif :**

Il demande à l'utilisateur de fournir directement dans **le terminal** les informations suivantes : 

- choix du nom du groupe de ressources
- choix du nom du compte de stockage
- choix du nom du conteneur

1. Il vérifie si le groupe de ressources existe, si oui il demande à l'utilisateur s'il souhaite l'utiliser.

2. Il procède a une validation des informations fournies par l'utilisateur.

3. Il ajoute un **timestamp au nom du compte de stockage pour le rendre unique**.

4. Aprés la création du compte de stockage, il **ajoute une règle de pare-feu pour autoriser l'accès au compte de stockage avec l'adresse IP publique** du poste depuis lequel le script est exécuté.

5. Il recupére la clé du compte de stockage et la stocke dans une variable d'environnement **"ARM_ACCESS_KEY"** sur votre poste

6. Après la création du conteneur, il crée un fichier `backend.tf` qui contient les informations pour stocker le tfstate dans le backend Azure.

7. Copier ce fichier dans le répertoire où se trouve votre code Terraform.

8. Pour finir `terraform init` pour initialiser le backend Azure.


## Notes : 

Résultat du fichier backend.tf :

```hcl
terraform {
  backend "azurerm" {
    resource_group_name   = "nom_de_votre_rg"
    storage_account_name  = "nom_de_votre_storage_account"
    container_name        = "nom_de_votre_container"
    key                   = "terraform.tfstate"
  }
}
```

Ce script utilise `ifconfig.me` pour obtenir l'adresse IP publique du poste.

En cas d'erreur, si le scripts s'arrete il fait appel a une fonction `clean_up` pour supprimer les ressources créées.

Pour voir les logs vous pouvez les rediriger vers un fichier en utilisant la commande `./backend.sh > backend.log 2>&1` ou `./backend.sh 2> error.log`


## CONCLUSION

Désormais le tfstate est stocké dans un conteneur Azure Storage.

Ce qui permet d'avoir une sauvegarde des données d'état en cas de perte du fichier local et collaborer à plusieurs sur le même code Terraform.

## Auteur

- [Kingston-run](https://gitlab.com/Kingston-run)