#!/bin/bash -xe

# Ce script crée un compte de stockage Azure, un conteneur et un fichier backend.tf pour stocker le tfstate de Terraform.

# prérequis :
# - Azure CLI doit être installé sur votre machine.
# - Vous devez être connecté à Azure CLI en utilisant la commande az login.
# - curl doit être installé sur votre machine.

# N'oubliez pas de rendre le script exécutable en utilisant la commande chmod +x backend.sh.
# Pour voir les logs les rediriger vers un fichier en utilisant la commande ./backend.sh > backend.log 2>&1. ou ./backend.sh 2> error.log

# Fonction pour nettoyer les ressources en cas d'erreur
cleanup() {
    echo "Une erreur s'est produite. Nettoyage des ressources..."
    az storage account delete --name "$STORAGE_ACCOUNT_NAME" --resource-group "$RESOURCE_GROUP_NAME" --yes
}

# Appeler la fonction cleanup en cas d'erreur
trap cleanup ERR


# Récupérer l'adresse IP publique du poste
echo "Récupération de l'adresse IP publique..."
IP_PUBLIC=$(curl -s ifconfig.me)

# Demander à l'utilisateur de renseigner les informations
echo "Veuillez entrer les informations requises..."
read -rp "Entrez le nom du groupe de ressources : " RESOURCE_GROUP_NAME
read -rp "Entrez le nom du compte de stockage : " STORAGE_ACCOUNT_NAME
read -rp "Entrez le nom du conteneur : " CONTAINER_NAME

# Validation du nom du compte de stockage
echo "Validation du nom du compte de stockage..."
if [[ ${#STORAGE_ACCOUNT_NAME} -lt 1 || ${#STORAGE_ACCOUNT_NAME} -gt 24 || "$STORAGE_ACCOUNT_NAME" != "${STORAGE_ACCOUNT_NAME,,}" ]]; then
    echo "Le nom du compte de stockage doit avoir entre 1 et 24 caractères et être en minuscules."
    exit 1
fi

# Ajouter un timestamp à la fin du nom du compte de stockage
echo "Ajout d'un timestamp au nom du compte de stockage..."
TIMESTAMP=$(date +%d%m%y)
STORAGE_ACCOUNT_NAME+=$TIMESTAMP

# Variables par défaut à utiliser si l'utilisateur ne fournit pas de valeurs
echo "Définition des variables par défaut..."
ACCESS_TIER="Hot"
SKU="Standard_LRS"
MIN_TLS_VERSION="TLS1_2"
PUBLIC_NETWORK_ACCESS_ENABLED=true
DEFAULT_ACTION="allow"
KIND="StorageV2"
CONTAINER_ACCESS_TYPE="blob"

# Vérifier si le groupe de ressources existe déjà
echo "Vérification de l'existence du groupe de ressources..."
if az group exists --name "$RESOURCE_GROUP_NAME"; then
    read -rp "Le groupe de ressources $RESOURCE_GROUP_NAME existe déjà. Voulez-vous l'utiliser ? (y/n) : " USE_EXISTING_RG
    if [[ $USE_EXISTING_RG != 'y' ]]; then
        echo "Veuillez fournir un nom de groupe de ressources différent."
        exit 1
    else
        # Obtenir la localisation du groupe de ressources existant
        LOCATION=$(az group show --name "$RESOURCE_GROUP_NAME" --query location -o tsv)
    fi
else
    # Demander à l'utilisateur de renseigner la localisation
    read -rp "Entrez la localisation : " LOCATION
    # Créer un groupe de ressources
    az group create \
        --name "$RESOURCE_GROUP_NAME" \
        --location "$LOCATION"
fi

# Vérifier si le compte de stockage existe déjà
echo "Vérification de l'existence du compte de stockage..."
STORAGE_ACCOUNT_EXIST=$(az storage account check-name --name "$STORAGE_ACCOUNT_NAME" --query nameAvailable -o tsv)

if [[ $STORAGE_ACCOUNT_EXIST == "false" ]]; then
    echo "Le compte de stockage $STORAGE_ACCOUNT_NAME existe déjà. Veuillez fournir un nom de compte de stockage différent."
    exit 1
fi

# Créer un compte de stockage
echo "Création du compte de stockage..."
az storage account create \
    --name "$STORAGE_ACCOUNT_NAME" \
    --resource-group "$RESOURCE_GROUP_NAME" \
    --location "$LOCATION" \
    --sku "$SKU" \
    --kind "$KIND" \
    --access-tier "$ACCESS_TIER" \
    --min-tls-version "$MIN_TLS_VERSION" \
    --allow-blob-public-access "$PUBLIC_NETWORK_ACCESS_ENABLED"

# Attendre que le compte de stockage soit créé avant d'ajouter une règle de réseau
echo "Attente de la création du compte de stockage..."
while [ "$(az storage account show --name "$STORAGE_ACCOUNT_NAME" --query provisioningState -o tsv)" != "Succeeded" ]; do
    sleep 10
done

# Ajouter une règle de réseau pour autoriser l'adresse IP publique du poste
echo "Ajout d'une règle de réseau..."
az storage account network-rule add \
    --account-name "$STORAGE_ACCOUNT_NAME" \
    --resource-group "$RESOURCE_GROUP_NAME" \
    --ip-address "$IP_PUBLIC" \
    --action "$DEFAULT_ACTION"

# Obtenir la clé de compte de stockage et la définir dans une variable d'environnement
echo "Récupération de la clé de compte de stockage..."
ACCOUNT_KEY=$(az storage account keys list --resource-group "$RESOURCE_GROUP_NAME" --account-name "$STORAGE_ACCOUNT_NAME" --query "[0].value" -o tsv)
echo "export ARM_ACCESS_KEY=$ACCOUNT_KEY" >> ~/.bashrc

# Créer un conteneur
echo "Création d'un conteneur..."
CONTAINER_CREATION=$(az storage container create \
    --name "$CONTAINER_NAME" \
    --account-key "$ACCOUNT_KEY" \
    --account-name "$STORAGE_ACCOUNT_NAME" \
    --fail-on-exist \
    --timeout 30 \
    --public-access "$CONTAINER_ACCESS_TYPE" 2>&1)
  
    # autres parametres possibles
    # --auth-mode "key or login" : spécifie le mode d'authentification à utiliser. Les valeurs valides sont key et login.
    # --connection-string : La chaîne de connexion à utiliser pour l'authentification.
    # --metadata : Métadonnées à associer au conteneur sous forme de paires clé-valeur.


if [[ $CONTAINER_CREATION != *"\"created\": true"* ]]; then
    echo "La création du conteneur a échoué."
    echo "Message d'erreur : $CONTAINER_CREATION"
    cleanup
    exit 1
fi


# Créer le fichier backend.tf
echo "Création du fichier backend.tf..."
echo "terraform {
  backend \"azurerm\" {
    resource_group_name  = \"$RESOURCE_GROUP_NAME\"
    storage_account_name = \"$STORAGE_ACCOUNT_NAME\"
    container_name       = \"$CONTAINER_NAME\"
    key                  = \"terraform.tfstate\"
  }
}" >backend.tf

echo "Le script a terminé avec succès !"
